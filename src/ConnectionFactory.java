import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ConnectionFactory{
	Connection conn;
	public ConnectionFactory(){
		try{
			conn= DriverManager.
				getConnection("jdbc:mysql://localhost/ExampleDb","root","root");
		}
		catch( SQLException e){
			throw new Error(e.getMessage());
		}
	}
	public ArrayList<Object[]> Select(){
			ArrayList<Object[]> data=new ArrayList<>();
		try{
			Statement stmt=conn.createStatement();
			ResultSet resultSet= stmt.executeQuery("Select * from tableName");
			int columnCount=resultSet.getMetaData().getColumnCount();
			while (resultSet.next()) {
				Object[] values = new Object[columnCount];
				for (int i = 1; i <= columnCount; i++) 
					values[i - 1] = resultSet.getObject(i);
				data.add(values);
			}
		}
		catch( SQLException e){
			throw new Error(e.getMessage());
		}
		return data;
	}
	public void Update(int fieldvalue,int id){
		try{
			PreparedStatement stmt=conn. //same logic with insert
				prepareStatement("update `tableName` SET field=? WHERE id=?");
			stmt.setInt(1,fieldvalue);
			stmt.setInt(2,id);
			stmt.executeUpdate();//executeUpdate is to return nothing
		}
		catch( SQLException e){
			throw new Error(e.getMessage());
		}
	}
}
